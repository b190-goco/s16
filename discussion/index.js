console.log("hello world");
/*

*/

// ARITHMETIC OPERATIONS SECTION

// creation of variable used in mathematics operations
let x = 1397, y = 7831;

let sum = x+y;
console.log(sum);

let diff = x-y;
console.log(diff);

let product = x*y;
console.log(product);

let quotient = x/y;
console.log(quotient);

let modulus = y % x;
console.log(modulus);

// ASSIGNMENT OPERATOR
let assignN = 8;

// ADDITION ASSIGNMENT
/*assignN = assignN + 2;*/

// SHORTHAND ASSIGNMENT
assignN += 2;
console.log(assignN);
assignN -= 2;
console.log(assignN);
assignN *= 2;
console.log(assignN);
assignN /= 2;
console.log(assignN);
assignN %= 2;
console.log(assignN);

// MULTIPLE OPERATORS AND PARENTHESIS
let mdas = 1+2-3*4/5;
console.log(mdas);

let pemdas = 1 + (2-3) * 4/5;
console.log(pemdas);

pemdas = (1 + (2-3)) * (4/5);
console.log(pemdas);

// INCREMENT AND DECREMENT SECTION
let z = 1;
// pre-increment
let i = ++z;
console.log("i++ " + i);
console.log("i++ " + z);
// post-increment
i = z++;
console.log("i-- " + i);
console.log("i-- " + z);

let d = --z;
console.log("i++ " + d);
console.log("i++ " + z);
// post-increment
d = z--;
console.log("i-- " + d);
console.log("i-- " + z);

// TYPE COERCION
// conversion of one data type to another
let a = '10';
let b = 12;
let coercion = a + b;
console.log(coercion);
// converted to STRING

/*MINI ACTIVITY*/

let j = 15; k = true, l = "pop!";

// NON-COERCION
// when same data types are joined
console.log(j+j);
// BOOL + NUM
console.log(j+k);
// STRING + BOOL
console.log(k+l);

// COMPARISON OPERATORS

// equality
console.log(1==1);
console.log(1==2);
console.log(1=="1");
console.log(1==true);
console.log('juan'=='juan');
let juan = 'juan';
console.log('juan'==juan);
// returns true/false

// inequality
console.log(1!=1);
console.log(1!=2);
console.log(1!='1');
console.log(0!=false);
console.log('juan'!='juan');
console.log('juan'!=juan);

// STRICT equality
console.log(1===1);
console.log(1===2);
console.log(1==="1");
console.log(1===true);
console.log('juan'==='juan');
console.log('juan'===juan);
// returns true/false

// STRICT inequality
console.log(1!==1);
console.log(1!==2);
console.log(1!=='1');
console.log(0!==false);
console.log('juan'!=='juan');
console.log('juan'!==juan);

// relational
a = 50;
b = 65;

let gt=a>b;
let lt=a<b;
let gtoet=a>=b;
let ltoet=a<=b;
console.log(gt);
console.log(lt);
console.log(gtoet);
console.log(ltoet);
// true or false output

let numstr = "30";
console.log(a>numstr);
// forced coercion leads to true
let str = "twenty";
console.log(b<=str);
// no coercion, non-numeric = NaN, false either way

// LOGICAL OPERATORS

// and
let isLeg = true, isReg = false;
let isReq = isReg&&isLeg;
console.log("Result of AND: "+isReq);

// or
let someReq = isReg || isLeg;
console.log("Result of OR: "+someReq);

// not
let someReqNM = !isReg;
console.log("Result of NOT: "+someReqNM);

